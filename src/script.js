// Теоретичні питання
// 1. Як можна створити рядок у JavaScript?
// Обгортаючи текст динарними '' , подвійними "" лапками , або обертками ``.

// 2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
//Різниці між одинарними (''), подвійними ("") як такою немає, потрібно використовувати ті, про які домовились на проекті;
//Щодо зворотніх (``) лапок, то використовуючи їх ми можемо вставлти посилпння на якусь змінну використовуючи ${},
//та можемо відображати текст на дукількох рядках без використання \n

// 3. Як перевірити, чи два рядки рівні між собою?
// console.log(string1 === string2);

// 4. Що повертає Date.now()?
//Поточну дату. 

// 5. Чим відрізняється Date.now() від new Date()?
// Date.now() - повертає сьогоднішню дату, new Date() - задає нову дату





// Практичні завдання
// 1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом

// (читається однаково зліва направо і справа наліво), або false в іншому випадку.

function isPalindrome(str){
    let reverse = '';
    for (let i = str.length - 1; i >= 0; i--) {
        reverse += str[i];
    }
    if (reverse === str) {
        return true;
    } else {
        return false;
    }
}

let str1 = 'madam';
let str2 = 'mister';
let str3 = 'kayak';

console.log(isPalindrome(str1));
console.log(isPalindrome(str2));
console.log(isPalindrome(str3));



// 2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, 
//максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, 
//і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:



// // Рядок коротше 20 символів

// funcName('checked string', 20); // true

// // Довжина рядка дорівнює 18 символів

// funcName('checked string', 10); // false

function strLength(string) {
    if (userPassword.length < 12) {
        console.log('The number of symbols must be 12 or more.');
        return false;
    } else {
        console.log('You password is accepted.');
        return true;
    }
}
let userPassword = prompt('Please create your pasword. It must consist of not shorter than 12 symbols');
console.log(strLength(userPassword));



// 3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt.
// Функція повина повертати значення повних років на дату виклику функцію.

let userBirthDateInput = prompt('Please enter you date of birth in the following way: YYYY/MM/DD.');
let userBirthDate = new Date(userBirthDateInput);
console.log(userBirthDate);

function getAge(dateString) {
    let today = new Date();
    let birthDate = new Date(dateString);
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}
console.log(getAge(userBirthDate))

